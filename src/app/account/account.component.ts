import { Component, OnInit, Input } from '@angular/core';
import { AccountsService } from '../accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  // providers: [LoggingService]
})
export class AccountComponent {

  @Input() account: { name: string, status: string };
  @Input() id: number;

  constructor(private accountService: AccountsService) { }

  onSetTo(status: string) {
    this.accountService.updateAccountStatus(this.id, status);
    this.accountService.changeStaus.emit(status);
    // this.loggingServie.logStatusChange(status);
  }

}
